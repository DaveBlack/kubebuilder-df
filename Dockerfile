# Use distroless as minimal base image to package the manager binary
# Refer to https://github.com/GoogleContainerTools/distroless for more details
FROM alpine:3.17
WORKDIR /
COPY bin/manager .
COPY dfekete-0.1.0.tgz /tmp

USER 65532:65532

ENTRYPOINT ["/manager"]
