/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// DfeketeSpec defines the desired state of Dfekete
type DfeketeSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Foo is an example field of Dfekete. Edit dfekete_types.go to remove/update
	// +kubebuilder:validation:Pattern:=^/tmp/.*tgz$

	HelmchartPath string      `json:"helmchartPath,omitempty"`
	Namespace     string      `json:"namespace"`
	Restart       metav1.Time `json:"restart"`
}

// DfeketeStatus defines the observed state of Dfekete
type DfeketeStatus struct {
	Restart            metav1.Time `json:"restart"`
	InstalledHelmChart string      `json:"InstalledHelmChart"`
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// Dfekete is the Schema for the dfeketes API
type Dfekete struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   DfeketeSpec   `json:"spec,omitempty"`
	Status DfeketeStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// DfeketeList contains a list of Dfekete
type DfeketeList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Dfekete `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Dfekete{}, &DfeketeList{})
}
