/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"time"

	"github.com/go-logr/logr"
	helmClient "github.com/mittwald/go-helm-client"

	//"google.golang.org/genproto/googleapis/rpc/status"

	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"

	//"k8s.io/client-go/scale/scheme/appsv1beta1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/predicate"

	goacademytsystemscomv1 "gitlab.com/DaveBlack/kubebuilder-df/api/v1"
	apps "k8s.io/api/apps/v1"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

// DfeketeReconciler reconciles a Dfekete object
type DfeketeReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

const myFinalizerName = "dfekete-operator"

//+kubebuilder:rbac:groups=goacademy.t-systems.com,resources=dfeketes,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=goacademy.t-systems.com,resources=dfeketes/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=goacademy.t-systems.com,resources=dfeketes/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the Dfekete object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.14.1/pkg/reconcile
func (r *DfeketeReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	logger := log.FromContext(ctx)
	logger.Info("Starting process of CR")

	var test goacademytsystemscomv1.Dfekete
	err := r.Client.Get(ctx, req.NamespacedName, &test) //reads coustom resorce > and writes to &test

	if err != nil {
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	opt := &helmClient.Options{
		Namespace: test.Spec.Namespace,
		Debug:     true,
		Linting:   false,
		DebugLog:  func(format string, a ...any) { fmt.Printf(format, a...) },
	}

	// Define the chart to be installed
	chartSpec := &helmClient.ChartSpec{
		ReleaseName: "dfekete-operator",
		ChartName:   test.Spec.HelmchartPath,
		Namespace:   test.Spec.Namespace,
		UpgradeCRDs: true,
		Wait:        true,
		Timeout:     5 * time.Minute,
	}

	logger.Info(chartSpec.Namespace)
	logger.Info(chartSpec.ChartName)

	helmClient, err := helmClient.New(opt)
	if err != nil {
		logger.Error(err, "")
		return ctrl.Result{}, err
	}

	{ // add the condition that the version must be different from 1

	}

	if !test.DeletionTimestamp.IsZero() {
		err = r.UninstallHelm(helmClient, chartSpec, logger, ctx, test, req)
	} else if test.Spec.HelmchartPath != test.Status.InstalledHelmChart {
		err = r.InstallHelm(helmClient, chartSpec, logger, ctx, test, req)
		if err == nil {
			return ctrl.Result{Requeue: true}, nil
		}
	} else if !test.Spec.Restart.Equal(&test.Status.Restart) && test.Generation > 1 {
		err = r.RestartHelm(helmClient, chartSpec, logger, ctx, test, req)
	}
	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *DfeketeReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&goacademytsystemscomv1.Dfekete{}).
		WithEventFilter(predicate.GenerationChangedPredicate{}).
		Complete(r)
}

func (r *DfeketeReconciler) UninstallHelm(helmClient helmClient.Client, chartSpec *helmClient.ChartSpec, logger logr.Logger, ctx context.Context, test goacademytsystemscomv1.Dfekete, req ctrl.Request) error {
	logger.Info("uninstaller")

	// Uninstall the chart release.
	// Note that helmclient.Options.Namespace should ideally match the namespace in chartSpec.Namespace.

	if err := helmClient.UninstallRelease(chartSpec); err != nil {
		logger.Error(err, "uninstaller error")
		return err
	}
	patch := client.MergeFrom(test.DeepCopy())
	if controllerutil.ContainsFinalizer(&test, myFinalizerName) {
		//logger.Info("removing finalizer")
		controllerutil.RemoveFinalizer(&test, myFinalizerName)
		err := r.Patch(ctx, &test, patch)
		logger.Error(err, "finalizer error")
	}
	return nil
}

func (r *DfeketeReconciler) InstallHelm(helmClient helmClient.Client, chartSpec *helmClient.ChartSpec, logger logr.Logger, ctx context.Context, test goacademytsystemscomv1.Dfekete, req ctrl.Request) error {
	// Install a chart release.
	// Note that helmclient.Options.Namespace should ideally match the namespace in chartSpec.Namespace.
	logger.Info("installing")
	if _, err := helmClient.InstallOrUpgradeChart(context.Background(), chartSpec, nil); err != nil {
		logger.Error(err, "installing error")
		return err
	}
	// finalizer je ochrana objektu pred tym nez sa spracuje

	logger.Info("handling finalizer")
	patch := client.MergeFrom(test.DeepCopy())
	if !controllerutil.ContainsFinalizer(&test, myFinalizerName) {
		controllerutil.AddFinalizer(&test, myFinalizerName)
		logger.Info("adding finalizer")
		if err := r.Patch(ctx, &test, patch); err != nil {
			logger.Error(err, "adding finalizer error")
			return err
		}
	}
	return nil
}

func (r *DfeketeReconciler) RestartHelm(helmClient helmClient.Client, chartSpec *helmClient.ChartSpec, logger logr.Logger, ctx context.Context, test goacademytsystemscomv1.Dfekete, req ctrl.Request) error {
	// add the condition that the version must be different from 1
	err := r.Client.Get(ctx, req.NamespacedName, &test) //reads coustom resorce > and writes to &test
	var Deploy2 apps.Deployment
	logger.Error(err, "restart deployment created")
	err2 := r.Client.Get(ctx, types.NamespacedName{Name: "bestapi", Namespace: test.Spec.Namespace}, &Deploy2) // cannot put deployment to CR test,,,must go to Deploy2 deployment

	if err2 != nil {
		logger.Error(err, "restart client.get failed")

		return client.IgnoreNotFound(err)
	}

	if Deploy2.Spec.Template.Annotations == nil {
		logger.Error(err, "restart annotation created")
		Deploy2.Spec.Template.Annotations = make(map[string]string)
	}

	Deploy2.Spec.Template.Annotations["restart"] = time.Now().Format(time.RFC3339)
	r.Client.Update(ctx, &Deploy2) // updating deployment
	logger.Error(err, "restart addded annotation")
	if err != nil {
		logger.Error(err, "restart update failed")
		return err

	}

	patch := client.MergeFrom(test.DeepCopy())
	test.Status.Restart = test.Spec.Restart
	logger.Error(err, "restart addded annotation")
	err = r.Status().Patch(ctx, &test, patch) // updateujem CR iba v status casti
	if err != nil {
		logger.Error(err, "restart update")
		//test.Status.Restart = test.Spec.Restart
		return err
	}
	return nil // ukoncujem loop pre restart aby to neslo dalej
}
